﻿using System;
using System.Linq;
using Crystalline.Core;
using Crystalline.Core.Web;
using Crystalline.Core.Web.Factory;
using FluentAssertions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Xbehave;

namespace Crystalline.Tests.PageObjectModelSample
{
    public class GooglePageTests
    {
        private readonly IWebDriverFactory _driverFactory = new ManagedWebDriverFactory();

        [Scenario]
        public void SimpleGoogleSearchShouldReturnValues(IWebDriver driver, GooglePage googlePage, IWebElement results)
        {
            "Given I am on the google home page"._(() =>
            {
                driver = _driverFactory.GetLocalDriver(SupportedBrowser.Chrome);
                googlePage = new GooglePage(driver);
                googlePage.Load();
            });

            "When I search for 'microsoft'"._(() =>
            {
                googlePage.Search("microsoft");
                results = googlePage.WaitUntil(ExpectedConditions.ElementExists(By.CssSelector(".hd")));
            });

            "Then the results section should load with the first result as the Microsoft home page"._(() =>
            {
                googlePage.SearchResults()
                    .First()
                    .Contains("Microsoft – Official Home Page", StringComparison.CurrentCultureIgnoreCase)
                    .Should()
                    .BeTrue();
            });
        }
    }
}
