﻿using System.Collections.Generic;
using System.Linq;
using Crystalline.Core.Web;
using Crystalline.Core.Web.Forms;
using Crystalline.Core.Web.Forms.Fields;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Crystalline.Tests.PageObjectModelSample
{
    public class GooglePage : WebPage
    {        
        [FindsBy(How = How.Name, Using = "btnG")] 
        private IWebElement _googleSearchButton;

        [FindsBy(How = How.Name, Using = "q")]
        private IWebElement _searchBox;

        public GooglePage(IWebDriver driver, double? timeout = 30)
            : base(driver, "http://www.google.com", new DefaultFieldFactory(), timeout)
        {            
        }

        public GooglePage Search(string searchString)
        {            
            FieldFactory.BuildElement<TextBox>(_searchBox, null).SetValue(searchString);
            _googleSearchButton.Click();
            return this;
        }

        public List<string> SearchResults()
        {
            return Driver.FindElements(By.CssSelector("h3.r")).Select(x => x.Text).ToList();
        }
    }
}
