﻿using Crystalline.Core.Web.Forms.Fields;
using FluentAssertions;
using Moq;
using OpenQA.Selenium;
using Xbehave;

namespace Crystalline.Tests.Forms
{
    public class FormFieldsTests
    {
        [Scenario]
        public void CheckboxCheckedAttributeTest()
        {
            var webElement = new Mock<IWebElement>();
            var checkbox = new CheckBox(webElement.Object);            
            webElement.Setup(m => m.Selected).Returns(true);
            checkbox.Checked.Should().BeTrue();
            checkbox.GetValue().ShouldAllBeEquivalentTo("true");
        }

        // todo - add tests for all form field types
    }
}
