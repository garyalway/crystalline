﻿using Crystalline.Core.Web.Forms;
using Crystalline.Core.Web.Forms.Fields;
using FluentAssertions;
using Moq;
using OpenQA.Selenium;
using Xbehave;

namespace Crystalline.Tests.Forms
{
    public class DefaultFieldFactoryTests
    {
        private readonly IFieldFactory _fieldFactory = new DefaultFieldFactory();

        [Scenario]
        public void BuildTextBoxElement()
        {
            var mockElement = new Mock<IWebElement>();
            mockElement.Setup(m => m.TagName).Returns("input");
            mockElement.Setup(m => m.GetAttribute("type")).Returns("text");
            mockElement.Setup(m => m.GetAttribute("value")).Returns("some text");
            var textBox = _fieldFactory.BuildElement<TextBox>(mockElement.Object, null);
            textBox.Should().BeOfType<TextBox>();
            textBox.GetValue().ShouldAllBeEquivalentTo("some text");
        }

        [Scenario]
        public void BuildPasswordTextBoxElement()
        {
            var mockElement = new Mock<IWebElement>();
            mockElement.Setup(m => m.TagName).Returns("input");
            mockElement.Setup(m => m.GetAttribute("type")).Returns("password");
            mockElement.Setup(m => m.GetAttribute("value")).Returns("some text");
            var textBox = _fieldFactory.BuildElement<TextBox>(mockElement.Object, null);
            textBox.Should().BeOfType<TextBox>();
            textBox.GetValue().ShouldAllBeEquivalentTo("some text");
        }

        [Scenario]
        public void BuildEmailTextBoxElement()
        {
            var mockElement = new Mock<IWebElement>();
            mockElement.Setup(m => m.TagName).Returns("input");
            mockElement.Setup(m => m.GetAttribute("type")).Returns("email");
            mockElement.Setup(m => m.GetAttribute("value")).Returns("some text");
            var textBox = _fieldFactory.BuildElement<TextBox>(mockElement.Object, null);
            textBox.Should().BeOfType<TextBox>();
            textBox.GetValue().ShouldAllBeEquivalentTo("some text");
        }

        [Scenario]
        public void BuildTelTextBoxElement()
        {
            var mockElement = new Mock<IWebElement>();
            mockElement.Setup(m => m.TagName).Returns("input");
            mockElement.Setup(m => m.GetAttribute("type")).Returns("tel");
            mockElement.Setup(m => m.GetAttribute("value")).Returns("some text");
            var textBox = _fieldFactory.BuildElement<TextBox>(mockElement.Object, null);
            textBox.Should().BeOfType<TextBox>();
            textBox.GetValue().ShouldAllBeEquivalentTo("some text");
        }

        [Scenario]
        public void BuildNumberTextBoxElement()
        {
            var mockElement = new Mock<IWebElement>();
            mockElement.Setup(m => m.TagName).Returns("input");
            mockElement.Setup(m => m.GetAttribute("type")).Returns("number");
            mockElement.Setup(m => m.GetAttribute("value")).Returns("some text");
            var textBox = _fieldFactory.BuildElement<TextBox>(mockElement.Object, null);
            textBox.Should().BeOfType<TextBox>();
            textBox.GetValue().ShouldAllBeEquivalentTo("some text");
        }

        [Scenario]
        public void BuildDropDownListElement()
        {
            var mockElement = new Mock<IWebElement>();
            mockElement.Setup(m => m.TagName).Returns("select");
            var ddl = _fieldFactory.BuildElement<DropDownList>(mockElement.Object, null);
            ddl.Should().BeOfType<DropDownList>();
        }
    }
}
