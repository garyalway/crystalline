﻿using System;
using Crystalline.Core.Web;
using Crystalline.Core.Web.Factory;
using FluentAssertions;
using Xbehave;

namespace Crystalline.Tests.SeleniumServer
{
    public class SeleniumServerRemoteDriverTests : IDisposable
    {
        private readonly IWebDriverFactory _webDriverFactory;        

        public SeleniumServerRemoteDriverTests()
        {
            SeleniumServerUtil.StartServer();
            _webDriverFactory = new DefaultWebDriverFactory();
        }

        [Scenario]
        public void GetRemoteBrowser()
        {
            var driver = _webDriverFactory.GetRemoteDriver(SupportedBrowser.Chrome, "localhost");
            driver.Get("http://www.google.com");
            driver.Title.ShouldBeEquivalentTo("Google");
            driver.Quit();
        }

        public void Dispose()
        {
            SeleniumServerUtil.StopServer();
        }
    }
}
