﻿using System;
using System.Diagnostics;
using Crystalline.Core;

namespace Crystalline.Tests.SeleniumServer
{
    public static class SeleniumServerUtil
    {
        private const string SeleniumServerJavaCmdArguments = @"-Dwebdriver.chrome.driver=chromedriver.exe -Dwebdriver.ie.driver=IEDriverServer.exe -jar selenium-server-standalone-2.53.0.jar";
        private static int _processId;

        public static bool Started { get; private set; }

        public static void StartServer()
        {
            if (Started) return;

            try
            {
                var compareExe =
                    new Process
                    {
                        StartInfo = new ProcessStartInfo
                        {
                            FileName = "java.exe",
                            Arguments = SeleniumServerJavaCmdArguments,
                            WorkingDirectory = @".\Drivers\",
                            UseShellExecute = false,
                            RedirectStandardOutput = true,
                            RedirectStandardError = true,
                            CreateNoWindow = true
                        }
                    };
                Started = compareExe.Start();
                if (!Started)
                    throw new Exception();
                _processId = compareExe.Id;                
            }
            catch (Exception exp)
            {
                LogConfiguration.GetLogger(typeof(SeleniumServerUtil)).Error("Selenium server failed to start", exp);
                Started = false;
                throw;
            }
        }

        public static void StopServer()
        {
            if (!Started) return;

            try
            {
                var p = Process.GetProcessById(_processId);
                p.Kill();
            }
            catch (Exception exp)
            {
                LogConfiguration.GetLogger(typeof(SeleniumServerUtil)).Warn("Selenium server failed to stop", exp);
            }
            finally
            {
                Started = false;
            }
        }
    }
}
