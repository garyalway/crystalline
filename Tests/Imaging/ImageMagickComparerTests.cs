﻿using System.Drawing;
using Crystalline.Core.Imaging;
using FluentAssertions;
using Xbehave;

namespace Crystalline.Tests.Imaging
{
    
    public class ImageMagickComparerTests
    {
        private readonly IImageComparer _imageUtil = new ImageMagickComparer();

        [Scenario]
        public void NormalizeImageDimensionsResizesTwoUnequalImagesReadyForComparison(Image image1, Image image2)
        {
            "Given there are two images of unequal size in height and width"
                ._(() =>
                {
                    image1 = new Bitmap(123, 456);
                    image2 = new Bitmap(789, 123);
                });

            "When the images are normalized ready for comparison"
                ._(() =>
                {
                    _imageUtil.NormalizeImageDimensions(ref image1, ref image2);
                });

            "Then the image dimensions are both normalized to be that of the larger dimension"
                ._(() =>
                {
                    image1.Width.ShouldBeEquivalentTo(789);
                    image1.Height.ShouldBeEquivalentTo(456);

                    image2.Width.ShouldBeEquivalentTo(789);
                    image2.Height.ShouldBeEquivalentTo(456);
                });
        }

        [Scenario]
        public void CombineTwoImagesSideBySide(Image image1, Image image2, Image combinedImage)
        {
            "Given there are two images of unequal size"
                ._(() =>
                {
                    image1 = new Bitmap(123, 456);
                    image2 = new Bitmap(789, 123);
                });

            "When the images are combined side by side"
                ._(() =>
                {
                    combinedImage = _imageUtil.CombineImages(image1, image2);
                });

            "Then the new combined, side by side image is twice the widest image and the same height as the tallest image"
                ._(() =>
                {
                    combinedImage.Width.ShouldBeEquivalentTo(789 * 2);
                    combinedImage.Height.ShouldBeEquivalentTo(456);
                });
        }

        [Scenario]
        public void AdjustImageDimensionsTest(Image image, Image newImage)
        {
            "Given an image"
                ._(() =>
                {
                    image = new Bitmap(1, 2);
                });

            "When the image dimensions are adjusted"
                ._(() =>
                {
                    newImage = _imageUtil.AdjustImageDimensions(image, 3, 4);
                });

            "Then the image dimensions are adjusted as specified"
                ._(() =>
                {
                    newImage.Width.ShouldBeEquivalentTo(3);
                    newImage.Height.ShouldBeEquivalentTo(4);
                });
        }

        [Scenario]
        public void ComparingTwoIdenticalImagesReturnsZero(Image image1, Image image2, Image diffImage, double distortion)
        {
            "Given two identical images of solid blue"
                ._(() =>
                {
                    image1 = new Bitmap(500, 500);

                    using (var g = Graphics.FromImage(image1))
                    {
                        g.FillRectangle(new SolidBrush(Color.Blue), new Rectangle(100, 100, 200, 200));
                        g.DrawImage(image1, new Point(0, 0));
                    }

                    image2 = image1;
                });

            "When the two images are compared"
                ._(() =>
                {                    
                    distortion = _imageUtil.CompareImages(image1, image2, out diffImage);
                });

            "Then the distortion factor should be 0"
                ._(() =>
                {
                    diffImage.Should().NotBeNull();
                    distortion.ShouldBeEquivalentTo(0);
                });
        }

        [Scenario]
        public void ComparingTwoDifferentImagesReturnsValueLargerThanZero(Image image1, Image image2, Image diffImage, double distortion)
        {
            "Given two images of the same dimensions but different fill colours"
                ._(() =>
                {
                    image1 = (Image) new Bitmap(500, 500);
                    using (var g = Graphics.FromImage(image1))
                    {
                        g.FillRectangle(new SolidBrush(Color.Blue), new Rectangle(100, 100, 200, 200));
                        g.DrawImage(image1, new Point(0, 0));
                    }

                    image2 = (Image) new Bitmap(500, 500);
                    using (var g = Graphics.FromImage(image2))
                    {
                        g.FillRectangle(new SolidBrush(Color.Red), new Rectangle(200, 200, 150, 150));
                        g.DrawImage(image2, new Point(0, 0));
                    }
                });

            "When the two images are compared"
                ._(() =>
                {
                    distortion = _imageUtil.CompareImages(image1, image2, out diffImage);
                });

            "Then the distortion factor should be greater than 0"
                ._(() =>
                {
                    diffImage.Should().NotBeNull();
                    distortion.Should().BeGreaterThan(0);
                });
        }
    }
}