﻿using System.Drawing;
using System.Drawing.Imaging;
using Crystalline.Core.Imaging;
using FluentAssertions;
using Xbehave;

namespace Crystalline.Tests.Imaging
{
    public class ImageExtensionMethodTests
    {
        [Scenario]
        public void ImageToByteArrayTest()
        {
            var image1 = (Image)new Bitmap(123, 456);
            var imageAsByteArray = image1.ToByteArray(ImageFormat.Png);
            imageAsByteArray.Should().NotBeNull();            
        }

        [Scenario]
        public void ByteArrayToImageTest()
        {
            var image1 = (Image)new Bitmap(123, 456);
            var imageAsByteArray = image1.ToByteArray(ImageFormat.Png);
            var image2 = imageAsByteArray.ToImage();
            image2.Should().NotBeNull();
            image1.Height.ShouldBeEquivalentTo(image2.Height);
            image1.Width.ShouldBeEquivalentTo(image2.Width);
        }

    }
}
