﻿using Crystalline.Core.Web;
using Crystalline.Core.Web.Factory;
using FluentAssertions;
using Xbehave;

namespace Crystalline.Tests.Web
{
    public class ManagedFactoryTestsLocalDriver
    {
        private readonly IWebDriverFactory _webDriverFactory;

        public ManagedFactoryTestsLocalDriver()
        {
            _webDriverFactory = new ManagedWebDriverFactory();
        }

        [Scenario]
        public void GetLocalChromeInstanceWithNoBrowserPreferences()
        {
            var browser = _webDriverFactory.GetLocalDriver(SupportedBrowser.Chrome);
            browser.Get("http://www.google.com");
            browser.Title.ShouldBeEquivalentTo("Google");
        }

        [Scenario]
        public void GetLocalInternetExplorerInstanceWithNoBrowserPreferences()
        {
            var browser = _webDriverFactory.GetLocalDriver(SupportedBrowser.InternetExplorer);
            browser.Get("http://www.google.com");
            browser.Title.ShouldBeEquivalentTo("Google");
        }

        [Scenario]
        public void GetLocalPhantomJsInstanceWithNoBrowserPreferences()
        {
            var browser = _webDriverFactory.GetLocalDriver(SupportedBrowser.PhantomJs);
            browser.Get("http://www.google.com");
            browser.Title.ShouldBeEquivalentTo("Google");
        }

        [Scenario(Skip="Not working with current local version of FireFox - issue #3")]
        public void GetLocalFireFoxInstanceWithNoBrowserPreferences()
        {
            var browser = _webDriverFactory.GetLocalDriver(SupportedBrowser.Firefox);
            browser.Get("http://www.google.com");
            browser.Title.ShouldBeEquivalentTo("Google");
        }
    }
}
