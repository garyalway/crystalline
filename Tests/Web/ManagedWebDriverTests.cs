﻿using Crystalline.Core.Web;
using FluentAssertions;
using Moq;
using OpenQA.Selenium;
using Xbehave;

namespace Crystalline.Tests.Web
{
    public class ManagedWebDriverTests
    {
        [Scenario]
        public void LocalChromeDoesNotSupportFullScreenshotsNatively()
        {
            var mockDriver = new Mock<IWebDriver>();
            var driver = new ManagedWebDriver(mockDriver.Object, SupportedBrowser.Chrome, false);
            driver.FullScreenshotIsNativelySupported().Should().BeFalse();
        }

        [Scenario]
        public void LocalInternetExplorerDoesNotSupportFullScreenshotsNatively()
        {
            var mockDriver = new Mock<IWebDriver>();
            var driver = new ManagedWebDriver(mockDriver.Object, SupportedBrowser.InternetExplorer, false);
            driver.FullScreenshotIsNativelySupported().Should().BeFalse();
        }

        [Scenario]
        public void LocalFirefoxSupportsFullScreenshotsNatively()
        {
            var mockDriver = new Mock<IWebDriver>();
            var driver = new ManagedWebDriver(mockDriver.Object, SupportedBrowser.Firefox, false);
            driver.FullScreenshotIsNativelySupported().Should().BeTrue();
        }

        [Scenario]
        public void LocalPhantomJsSupportsFullScreenshotsNatively()
        {
            var mockDriver = new Mock<IWebDriver>();
            var driver = new ManagedWebDriver(mockDriver.Object, SupportedBrowser.PhantomJs, false);
            driver.FullScreenshotIsNativelySupported().Should().BeTrue();
        }

    }
}
