﻿using System;
using System.IO;
using Crystalline.Core.Web;
using Crystalline.Core.Web.Factory;
using FluentAssertions;
using Xbehave;
using Xunit;

namespace Crystalline.Tests.Web
{
    
    public class WebPageTests : IDisposable
    {        
        private readonly WebPage _webPage;

        public WebPageTests()
        {
            _webPage = new WebPage(new DefaultWebDriverFactory().GetLocalDriver(SupportedBrowser.Chrome));
        }

        [Scenario]
        public void GetUserAgentTest()
        {
            var agent = _webPage.Driver.GetUserAgent();
            agent.Should().NotBeNullOrEmpty();            
        }

        [Scenario]
        public void GetDocumentReadyStateTest()
        {
            var readyState = _webPage.Driver.GetDocumentReadyState();
            readyState.Should().NotBeNullOrEmpty();            
        }

        [Scenario]
        public void GetBrowserVersionTest()
        {
            // go to a site that has JQuery
            _webPage.Driver.Get("http://www.microsoft.com");
            var version = _webPage.Driver.GetBrowserVersion();
            version.Should().NotBeNullOrEmpty();
        }

        [Scenario]
        public void SaveScreenShotWithTimeStampTest()
        {
            _webPage.Driver.Get("http://www.google.com");
            var imageSaved = new FileInfo(_webPage.SaveScreenShot(_webPage.GetScreenShot(), "WebPageTestScreenShotSaveWithTimeStamp", @".\Test"));
            imageSaved.Exists.Should().BeTrue();           
        }

        [Scenario]
        public void SaveScreenShotWithoutTimeStampTest()
        {
            _webPage.Driver.Get("http://www.google.com");
            var imageSaved = new FileInfo(_webPage.SaveScreenShot(_webPage.GetScreenShot(), "WebPageTestScreenShotSave", @".\Test", false));
            imageSaved.Exists.Should().BeTrue();            
        }

        [Scenario]
        public void SaveScreenShotInvalidFilePathTest()
        {
            _webPage.Driver.Get("http://www.google.com");                       
            Assert.Throws<DirectoryNotFoundException>(
                () => _webPage.SaveScreenShot(_webPage.GetScreenShot(), "FileName", @"X:\invalid file system"));
        }

        [Scenario]
        public void SaveScreenShotInvalidFileNameTest()
        {
            _webPage.Driver.Get("http://www.google.com");
            Assert.Throws<NotSupportedException>(
                () => _webPage.SaveScreenShot(_webPage.GetScreenShot(), "Invalid:$@FileName", @"C:\Testing\Test"));
        }

        public void Dispose()
        {
            if (_webPage != null)
                _webPage.Driver.Quit();
        }
    }
}
