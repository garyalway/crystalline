﻿using System;
using System.Collections.Generic;
using Crystalline.Core.Web;
using Crystalline.Core.Web.Factory;
using FluentAssertions;
using OpenQA.Selenium;
using Xbehave;

namespace Crystalline.Tests.Web
{
    public class DefaultFactoryTestsLocalDriver : IDisposable
    {
        private readonly IWebDriverFactory _webDriverFactory;
        private IWebDriver _driver;

        public DefaultFactoryTestsLocalDriver()
        {
            _webDriverFactory = new DefaultWebDriverFactory();
        }                

        [Scenario]
        public void GetLocalChromeInstanceWithNoBrowserPreferences()
        {
            _driver = _webDriverFactory.GetLocalDriver(SupportedBrowser.Chrome);
            _driver.Get("http://www.google.com");
            _driver.Title.ShouldBeEquivalentTo("Google");
        }

        [Scenario]
        public void GetLocalChromeInstanceWithBrowserPreferences()
        {
            var preferences = new Dictionary<string, object>
            {
                {"download.default_directory", @"C:\code"},
                {"download.prompt_for_download", false}
            };
            _driver = _webDriverFactory.GetLocalDriver(SupportedBrowser.Chrome, preferences);
            _driver.Should().NotBeNull();
        }

        [Scenario]
        public void GetLocalInternetExplorerInstanceWithNoBrowserPreferences()
        {
            _driver = _webDriverFactory.GetLocalDriver(SupportedBrowser.InternetExplorer);
            _driver.Get("http://www.google.com");
            _driver.Title.ShouldBeEquivalentTo("Google");
        }

        [Scenario]
        public void GetLocalPhantomJsInstanceWithNoBrowserPreferences()
        {
            _driver = _webDriverFactory.GetLocalDriver(SupportedBrowser.PhantomJs);
            _driver.Get("http://www.google.com");
            _driver.Title.ShouldBeEquivalentTo("Google");
        }

        [Scenario(Skip="Not working with current local version of FireFox - issue #3")]
        public void GetLocalFirefoxInstanceWithNoBrowserPreferences()
        {
            _driver = _webDriverFactory.GetLocalDriver(SupportedBrowser.Firefox);
            _driver.Get("http://www.google.com");
            _driver.Title.ShouldBeEquivalentTo("Google");
        }

        [Scenario(Skip = "Not working with current local version of FireFox - issue #3")]
        public void GetLocalFirefoxInstanceWithBrowserPreferences()
        {
            var preferences = new Dictionary<string, object>
            {
                {"download.default_directory", @"C:\code"},
                {"download.prompt_for_download", false}
            };
            _driver = _webDriverFactory.GetLocalDriver(SupportedBrowser.Firefox, preferences);
            _driver.Should().NotBeNull();
        }

        //        [Scenario]
        //        public void MultipleInstancesAreDistinctTest()
        //        {
        //            var driver1 = GetDriver1Instance();
        //            var driver2 = GetDriver2Instance();
        //            //Assert.AreNotEqual(driver1, driver2);
        //        }

        public void Dispose()
        {
            _driver.Quit();
        }
    }
}
