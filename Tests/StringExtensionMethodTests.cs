﻿using System;
using Crystalline.Core;
using FluentAssertions;
using Xbehave;

namespace Crystalline.Tests
{
    public class StringExtensionMethodTests
    {
        private const string Str1 = "A test string";
        private const string Str2 = "TEST";

        [Scenario]
        public void StringContainsExtensionMethodIgnoreCaseIsTrue()
        {
            Str1.Contains(Str2, StringComparison.OrdinalIgnoreCase).Should().BeTrue();            
        }

        [Scenario]
        public void StringContainsExtensionMethodDoNotIgnoreCaseIsFalse()
        {
            Str1.Contains(Str2, StringComparison.Ordinal).Should().BeFalse();            
        }

        [Scenario]
        public void StringContainsNativeShouldBeFalse()
        {
            // native String.Contains
            Str1.Contains(Str2).Should().BeFalse();
        }
    
    }
}
