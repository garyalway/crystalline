﻿using System.Drawing;

namespace Crystalline.Core.Imaging
{
    public interface IImageComparer
    {
        void NormalizeImageDimensions(ref Image image1, ref Image image2);
        Image AdjustImageDimensions(Image image, int newWidth, int newHeight);
        Image CombineImages(Image image1, Image image2);
        double CompareImages(Image image1, Image image2, out Image outImage);
        double CompareImages(Image image1, Image image2, ErrorMetric metric, Channels channels, out Image outImage);
    }
}
