﻿using System;
using System.Drawing;
using ImageMagick;

namespace Crystalline.Core.Imaging
{
    public enum ErrorMetric
    {
        Absolute,
        Fuzz,
        MeanAbsolute,
        MeanErrorPerPixel,
        MeanSquared,
        NormalizedCrossCorrelation,
        PeakAbsolute,
        PeakSignalToNoiseRatio,
        PerceptualHash,
        RootMeanSquared,
        Undefined
    }

    public enum Channels
    {
        All,
        Alpha,
        Black,
        Blue,
        Composite,
        Cyan,
        Default,
        Gray,
        Grays,
        Green,
        Index,
        Magenta,
        Opacity,
        Red,
        RGB,
        Sync,
        TrueAlpha,
        Undefined,
        Yellow
    }

    public class ImageMagickComparer : IImageComparer
    {
        public void NormalizeImageDimensions(ref Image image1, ref Image image2)
        {
            var height = Math.Max(image1.Height, image2.Height);
            var width = Math.Max(image1.Width, image2.Width);

            if (image1.Height != height || image1.Width != width)
            {
                image1 = AdjustImageDimensions(image1, width, height);
            }
            if (image2.Height != height || image2.Width != width)
            {
                image2 = AdjustImageDimensions(image2, width, height);
            }
        }

        public Image AdjustImageDimensions(Image image, int newWidth, int newHeight)
        {
            var canvas = new Bitmap(newWidth, newHeight);

            using (var g = Graphics.FromImage(canvas))
            {
                g.FillRectangle(new SolidBrush(Color.White), new Rectangle(0, 0, canvas.Width, canvas.Height));
                g.DrawImage(image, new Point(0, 0));
            }

            image.Dispose(); // unlock the image file

            return canvas;
        }

        public Image CombineImages(Image image1, Image image2)
        {
            NormalizeImageDimensions(ref image1, ref image2);

            var canvas = new Bitmap(image1.Width * 2, image1.Height);

            using (var g = Graphics.FromImage(canvas))
            {
                g.FillRectangle(new SolidBrush(Color.White), new Rectangle(0, 0, canvas.Width, canvas.Height));
                g.DrawImage(image1, new Point(0, 0));
                g.DrawImage(image2, new Point(image1.Width, 0));
            }

            return canvas;
        }

        public double CompareImages(Image image1, Image image2, out Image outImage)
        {
            return CompareImages(image1, image2, ErrorMetric.MeanAbsolute, Channels.All, out outImage);
        }

        public double CompareImages(Image image1, Image image2, ErrorMetric metric, Channels channels, out Image outImage)
        {
            NormalizeImageDimensions(ref image1, ref image2);

            var mImage1 = new MagickImage(new Bitmap(image1));
            var mImage2 = new MagickImage(new Bitmap(image2));
            var diffImage = new MagickImage();

            var imChannel = (ImageMagick.Channels)Enum.Parse(typeof(ImageMagick.Channels), channels.ToString());
            var immetric = (ImageMagick.ErrorMetric)Enum.Parse(typeof(ImageMagick.ErrorMetric), channels.ToString());
            var error = mImage1.Compare(mImage2, immetric, diffImage, imChannel);

            outImage = diffImage.ToBitmap();
            return error;
        }
    }
}
