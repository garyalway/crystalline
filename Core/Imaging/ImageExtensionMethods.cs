﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Crystalline.Core.Imaging
{
    public static class ImageExtensionMethods
    {
        public static Image ToImage(this byte[] byteArrayIn)
        {
            var ms = new MemoryStream(byteArrayIn);
            var returnImage = Image.FromStream(ms);
            return returnImage;
        }

        public static byte[] ToByteArray(this Image imageIn, ImageFormat imageFormat)
        {
            var ms = new MemoryStream();
            imageIn.Save(ms, imageFormat);
            return ms.ToArray();
        }
    }
}
