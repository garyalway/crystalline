﻿using System;
using log4net;

namespace Crystalline.Core
{
    public static class LogConfiguration
    {
        public static ILog GetLogger(Type T)
        {
            return LogManager.GetLogger(T);
        }

        static LogConfiguration()
        {
            log4net.Config.XmlConfigurator.Configure();
        }
    }
}
