﻿using System.Collections.ObjectModel;
using OpenQA.Selenium;

namespace Crystalline.Core.Web
{
    public class ManagedWebDriver : IWebDriver, ITakesScreenshot, IJavaScriptExecutor
    {
        private readonly IWebDriver _driverInstance;
        private readonly bool _isRemote;

        public SupportedBrowser BrowserType { get; private set; }

        public bool FullScreenshotIsNativelySupported()
        {
            if (_isRemote)
                return (BrowserType == SupportedBrowser.InternetExplorer || BrowserType == SupportedBrowser.Firefox);

            return (BrowserType == SupportedBrowser.PhantomJs || BrowserType == SupportedBrowser.Firefox);
        }

        public object ExecuteScript(string script, params object[] args)
        {
            return ((IJavaScriptExecutor)_driverInstance).ExecuteScript(script, args);
        }

        public object ExecuteAsyncScript(string script, params object[] args)
        {
            return ((IJavaScriptExecutor)_driverInstance).ExecuteAsyncScript(script, args);
        }

        public Screenshot GetScreenshot()
        {
            return ((ITakesScreenshot)_driverInstance).GetScreenshot();
        }

        public ManagedWebDriver(IWebDriver driver, SupportedBrowser browserType, bool remote)
        {
            _isRemote = remote;
            BrowserType = browserType;
            _driverInstance = driver;
        }

        public void Close()
        {
            _driverInstance.Close();
        }

        public string CurrentWindowHandle
        {
            get { return _driverInstance.CurrentWindowHandle; }
        }

        public IOptions Manage()
        {
            return _driverInstance.Manage();
        }

        public INavigation Navigate()
        {
            return _driverInstance.Navigate();
        }

        public string PageSource
        {
            get { return _driverInstance.PageSource; }
        }

        public void Quit()
        {
            _driverInstance.Quit();
        }

        public ITargetLocator SwitchTo()
        {
            return _driverInstance.SwitchTo();
        }

        public string Title
        {
            get { return _driverInstance.Title; }
        }

        public string Url
        {
            get
            {
                return _driverInstance.Url;
            }
            set { _driverInstance.Url = value; }
        }

        public ReadOnlyCollection<string> WindowHandles
        {
            get { return _driverInstance.WindowHandles; }
        }

        public IWebElement FindElement(By by)
        {
            return _driverInstance.FindElement(by);
        }

        public ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            return _driverInstance.FindElements(by);
        }

        public void Dispose()
        {
            _driverInstance.Dispose();
        }
    }
}