﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using OpenQA.Selenium;

namespace Crystalline.Core.Web
{
    internal class MultipleViewportConstructedScreenshot
    {
        private readonly IWebDriver _driverInstance;

        public MultipleViewportConstructedScreenshot(IWebDriver driver)
        {
            _driverInstance = driver;
        }

        public Image GetScreenshot()
        {
            // make sure viewport is scrolled to the top of the page before starting
            _driverInstance.ExecuteScript("window.scrollTo(0, 0)");
            Thread.Sleep(200);

            var documentSize = _driverInstance.GetDocumentBodySize();
            var viewPortSize = _driverInstance.GetDocumentViewPortSize();

            if (documentSize.Height < viewPortSize.Height)
            {
                documentSize.Height = viewPortSize.Height;
            }
            if (documentSize.Width < viewPortSize.Width)
            {
                documentSize.Width = viewPortSize.Width;
            }

            var rectangles = SplitDocumentScreenIntoRectangles(documentSize, viewPortSize);

            var fullScreenshot = new Bitmap(documentSize.Width, documentSize.Height);
            var previous = Rectangle.Empty;
            foreach (var rectangle in rectangles)
            {
                if (previous != Rectangle.Empty)
                {
                    _driverInstance.ExecuteScript(String.Format("window.scrollBy({0}, {1})", rectangle.Right - previous.Right, rectangle.Bottom - previous.Bottom));
                    Thread.Sleep(200);
                }

                var screenshot = _driverInstance.TakeScreenshot();
                var sourceRectangle = new Rectangle(viewPortSize.Width - rectangle.Width, viewPortSize.Height - rectangle.Height, rectangle.Width, rectangle.Height);

                using (var g = Graphics.FromImage(fullScreenshot))
                {
                    g.DrawImage(screenshot, rectangle, sourceRectangle, GraphicsUnit.Pixel);
                }

                previous = rectangle;
            }

            return fullScreenshot;
        }

        private static IEnumerable<Rectangle> SplitDocumentScreenIntoRectangles(Size documentSize, Size viewPortSize)
        {
            var rectangles = new List<Rectangle>();
            for (var i = 0; i < documentSize.Height; i += viewPortSize.Height)
            {
                var height = (i + viewPortSize.Height > documentSize.Height) ? (documentSize.Height - i) : viewPortSize.Height;
                for (var ii = 0; ii < documentSize.Width; ii += viewPortSize.Width)
                {
                    var width = (ii + viewPortSize.Width > documentSize.Width) ? (documentSize.Width - ii) : viewPortSize.Width;
                    rectangles.Add(new Rectangle(ii, i, width, height));
                }
            }
            return rectangles;
        }
    }
}
