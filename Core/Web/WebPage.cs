﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using Castle.Core.Internal;
using Crystalline.Core.Web.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace Crystalline.Core.Web
{
    public class WebPage
    {
        private double? _timeout;
        public IWebDriver Driver;
        public Uri Url;
        protected IFieldFactory FieldFactory;        
        public string BaseUrl { get { return Url.AbsoluteUri; } }
        
        public WebPage(IWebDriver driver, string url = null, IFieldFactory fieldFactory = null, double? timeout = null)
        {
            if (timeout.HasValue) Timeout = timeout.Value;
            if (url != null) Url = new Uri(url);
            FieldFactory = fieldFactory;
            Driver = driver;
            PageFactory.InitElements(Driver, this);
        }        

        public virtual WebPage Load()
        {
            if(Url == null || Url.AbsolutePath.IsNullOrEmpty())
                throw new ArgumentException("WebPage Url not set");
            Driver.Get(Url.AbsoluteUri);
            return this;
        }

        public double Timeout
        {
            get
            {
                return _timeout ?? 0;
            }
            set { _timeout = value; }
        }

        public virtual void ClearCookieMessage(By findBy)
        {
            try
            {
                var xs = Driver.FindElements(findBy);
                if (xs.Count > 0)
                    xs[0].Click();
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public T WaitUntil<T>(Func<IWebDriver, T> condition)
        {
            return WaitUntil(condition, Timeout);
        }

        public T WaitUntil<T>(Func<IWebDriver, T> condition, double timeoutValue)
        {
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeoutValue));
            wait.IgnoreExceptionTypes(typeof(StaleElementReferenceException));
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            
            return wait.Until(condition);
        }

        public Image GetScreenShot(bool fullScreen = true)
        {
            return fullScreen ? Driver.TakeFullScreenshot() : Driver.TakeScreenshot();
        }

        public string SaveScreenShot(Image screenShot, string description, string saveLocation, bool appendTimeStamp = true)
        {
            var sb = new StringBuilder();
            sb.Append(CreateDirectory(saveLocation).FullName.TrimEnd('\\'));
            sb.Append("\\");
            sb.Append(description);
            if (appendTimeStamp)
            {
                sb.Append("_");
                sb.Append(DateTime.Now.ToString(@"ddMMyyy_hhmm_ss"));
            }
            sb.Append(".png");

            screenShot.Save(sb.ToString(), ImageFormat.Png);
            return sb.ToString();
        }

        private DirectoryInfo CreateDirectory(string saveLocation)
        {
            var saveDirectoryLocation = new DirectoryInfo(saveLocation);
            if (saveDirectoryLocation.Exists) return saveDirectoryLocation;
            return Directory.CreateDirectory(saveLocation);
        }
    }
}
