﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Remote;

namespace Crystalline.Core.Web
{
    public static class WebDriverExtensions
    {
        public static string GetUserAgent(this IWebDriver driver)
        {
            return driver.ExecuteScript<string>("return navigator.userAgent");
        }

        public static string GetDocumentReadyState(this IWebDriver driver)
        {
            return driver.ExecuteScript<string>("return document.readyState");
        }

        public static string GetBrowserVersion(this IWebDriver driver)
        {
            return driver.ExecuteScript<string>("return $.browser.version");
        }

        public static void Get(this IWebDriver driver, string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        public static void Get(this IWebDriver driver, Uri uri)
        {
            driver.Get(uri.AbsoluteUri);
        }

        public static void ExecuteScript(this IWebDriver driver, string javascript)
        {
            var jsExec = (IJavaScriptExecutor)driver;
            jsExec.ExecuteScript(javascript);
        }

        public static T ExecuteScript<T>(this IWebDriver driver, string javascript)
        {
            var jsExec = (IJavaScriptExecutor)driver;
            return (T)jsExec.ExecuteScript(javascript);
        }

        public static Image TakeScreenshot(this IWebDriver driver)
        {
            Image screenshotImage;

            var webDriver = driver as RemoteWebDriver;
            var screenshot = webDriver != null ? webDriver.GetScreenshot() : ((ITakesScreenshot) driver).GetScreenshot();

            using (var memStream = new MemoryStream(screenshot.AsByteArray))
            {
                screenshotImage = Image.FromStream(memStream);
            }

            return screenshotImage;
        }

        public static Size GetDocumentBodySize(this IWebDriver driver)
        {
            var width = (int)driver.ExecuteScript<long>("return document.body.clientWidth");
            var height = (int)driver.ExecuteScript<long>("return document.body.clientHeight");
            return new Size(width, height);
        }

        public static Size GetDocumentViewPortSize(this IWebDriver driver)
        {
            var width = (int)driver.ExecuteScript<long>("return document.documentElement.clientWidth");
            var height = (int)driver.ExecuteScript<long>("return document.documentElement.clientHeight");
            return new Size(width, height);
        }

        private static bool FullScreenshotIsNativelySupported(IWebDriver driver)
        {
            // if using a managed webdriver factory
            var webDriverWithLogging = driver as ManagedWebDriver;
            if (webDriverWithLogging != null)
            {
                return webDriverWithLogging.FullScreenshotIsNativelySupported();
            }

            // otherwise: using the base selenium webdriver types
            var firefoxDriver = driver as FirefoxDriver;
            if (firefoxDriver != null)
                return true;

            var phantomJsDriver = driver as PhantomJSDriver;
            if (phantomJsDriver != null)
                return true;

            var internetExplorerDriver = driver as InternetExplorerDriver;
            if (internetExplorerDriver != null)
                return false;

            var chromeDriver = driver as ChromeDriver;
            if (chromeDriver != null)
                return false;

            var remoteDriver = driver as RemoteWebDriver;
            if (remoteDriver != null)
            {
                return !remoteDriver.Capabilities.BrowserName.Equals(SupportedBrowser.Chrome.ToString().ToLower());
            }

            var msg = string.Format("Browser type not supported: {0}", driver.GetType());
            throw new NotSupportedException(msg);
        }

        public static Image TakeFullScreenshot(this IWebDriver driver)
        {
            return FullScreenshotIsNativelySupported(driver)
                ? driver.TakeScreenshot()
                : (new MultipleViewportConstructedScreenshot(driver).GetScreenshot());
        }

        public static Dictionary<string, object> GetPerformanceTimings(this IWebDriver driver)
        {
            var webTiming = (Dictionary<string, object>)((IJavaScriptExecutor)driver)
                .ExecuteScript(@"var perf = window.performance || window.webkitPerformance || window.mozPerformance || window.msPerformance || {};
                                 var timings = perf.timing || {};
                                 return timings;");
            return webTiming;
        }
    }
}