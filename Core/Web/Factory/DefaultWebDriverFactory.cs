﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace Crystalline.Core.Web.Factory
{
    public class DefaultWebDriverFactory : AbstractWebDriverFactory, IWebDriverFactory
    {
        public IWebDriver GetRemoteDriver(SupportedBrowser browserType, string seleniumServerHostName)
        {
            return GetRemoteDriver(browserType, seleniumServerHostName, SeleniumServerPort);
        }

        public IWebDriver GetRemoteDriver(SupportedBrowser browserType, string seleniumServerHostName, int port)
        {
            return CreateRemoteWebDriverInstance(browserType, seleniumServerHostName, port);
        }

        public IWebDriver GetLocalDriver(SupportedBrowser browserType)
        {
            return GetLocalDriver(browserType, null);
        }

        public IWebDriver GetLocalDriver(SupportedBrowser browserType, Dictionary<string, object> preferences)
        {
            return CreateLocalDriverInstance(browserType, preferences);
        }        
    }
}
