﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace Crystalline.Core.Web.Factory
{
    public interface IWebDriverFactory
    {
        IWebDriver GetRemoteDriver(SupportedBrowser browserType, string seleniumServerHostName, int port);
        IWebDriver GetRemoteDriver(SupportedBrowser browserType, string seleniumServerHostName);

        IWebDriver GetLocalDriver(SupportedBrowser browserType, Dictionary<string, object> preferences);
        IWebDriver GetLocalDriver(SupportedBrowser browserType);
    }
}
