﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace Crystalline.Core.Web.Factory
{
    public class ManagedWebDriverFactory : AbstractWebDriverFactory, IWebDriverFactory
    {
        public ManagedWebDriverFactory()
        {
            AppDomain.CurrentDomain.DomainUnload += Dispose;
        }

        private List<IWebDriver> _instances = new List<IWebDriver>();

        public int BrowserInstanceCount
        {
            get { return _instances.Count; }
        }

        #region Remote Driver
        public IWebDriver GetRemoteDriver(SupportedBrowser browserType, string seleniumServerHostName)
        {
            return GetRemoteDriver(browserType, seleniumServerHostName, SeleniumServerPort);
        }

        public IWebDriver GetRemoteDriver(SupportedBrowser browserType, string seleniumServerHostName, int port)
        {
            var driver = CreateRemoteWebDriverInstance(browserType, seleniumServerHostName, port);
            _instances.Add(driver);
            return new ManagedWebDriver(driver, browserType, true);
        }
        #endregion

        #region Local Driver
        public IWebDriver GetLocalDriver(SupportedBrowser browserType)
        {
            return GetLocalDriver(browserType, null);
        }

        public IWebDriver GetLocalDriver(SupportedBrowser browserType, Dictionary<string, object> preferences)
        {
            var driver = CreateLocalDriverInstance(browserType, preferences);
            _instances.Add(driver);
            return new ManagedWebDriver(driver, browserType, false);
        }
        #endregion

        public void CloseAllDrivers()
        {
            foreach (var x in _instances.Where(x => x != null))
            {
                try
                {
                    x.Quit();
                }
                catch (Exception)
                {
                    // do not throw exceptions on dispose
                }
            }
            _instances.Clear();
        }

        private void Dispose(object sender, EventArgs args)
        {
            CloseAllDrivers();
            _instances = null;
        }
    }
}
