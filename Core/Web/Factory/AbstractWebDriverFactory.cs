﻿using System;
using System.Collections.Generic;
using System.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Remote;

namespace Crystalline.Core.Web.Factory
{
    public abstract class AbstractWebDriverFactory
    {
        protected static readonly int SeleniumServerPort = Convert.ToInt32(ConfigurationManager.AppSettings["SeleniumServerPort"]);
        protected static readonly string DriverExeDirectory = ConfigurationManager.AppSettings["DriverExeDirectory"];

        protected IWebDriver CreateLocalDriverInstance(SupportedBrowser browserType)
        {
            return CreateLocalDriverInstance(browserType, null);
        }

        protected IWebDriver CreateLocalDriverInstance(SupportedBrowser browserType, Dictionary<string, object> preferences)
        {
            var options = GetBrowserOptions(browserType, preferences);
            switch (browserType)
            {
                case SupportedBrowser.Chrome:
                    return new ChromeDriver(DriverExeDirectory, (ChromeOptions) options);
                case SupportedBrowser.Firefox:
                    return new FirefoxDriver((FirefoxProfile) options);
                case SupportedBrowser.InternetExplorer:
                    return new InternetExplorerDriver(DriverExeDirectory, (InternetExplorerOptions) options);
                case SupportedBrowser.PhantomJs:
                    return new PhantomJSDriver(DriverExeDirectory);
                default:
                    throw new NotSupportedException(browserType + " is not supported");
            }
        }

        protected static RemoteWebDriver CreateRemoteWebDriverInstance(SupportedBrowser browserType, string seleniumnServerHostName, int port)
        {
            var capabilities = GetCapabilities(browserType);
            var driver = new RemoteWebDriver(new Uri("http://" + seleniumnServerHostName + ":" + port + "/wd/hub"), capabilities);
            return driver;
        }

        protected static DesiredCapabilities GetCapabilities(SupportedBrowser browserType)
        {            
            switch (browserType)
            {
                case SupportedBrowser.Chrome:
                    return DesiredCapabilities.Chrome();
                case SupportedBrowser.Firefox:
                    return DesiredCapabilities.Firefox();
                case SupportedBrowser.InternetExplorer:                    
                    var capabilities = DesiredCapabilities.InternetExplorer();
                    capabilities.SetCapability(CapabilityType.TakesScreenshot, true);
                    capabilities.SetCapability("ignoreProtectedModeSettings", true);                    
                    return capabilities;
                case SupportedBrowser.PhantomJs:
                    return DesiredCapabilities.PhantomJS();
                default:
                    throw new NotSupportedException(browserType + " is not supported");
            }
        }

        private static object GetBrowserOptions(SupportedBrowser browserType, Dictionary<string, object> preferences)
        {
            object ret;
            switch (browserType)
            {
                case SupportedBrowser.Chrome:
                    var chromeoptions = new ChromeOptions();
                    if (preferences != null)
                        foreach (var prefKeyPair in preferences)
                            chromeoptions.AddUserProfilePreference(prefKeyPair.Key, prefKeyPair.Value);
                    ret = chromeoptions;
                    break;
                case SupportedBrowser.Firefox:
                    var firefoxoptions = new FirefoxProfile();
                    firefoxoptions.SetPreference("browser.private.browsing.autostart", true);
                    if (preferences != null)
                        foreach (var prefKeyPair in preferences)
                        {
                            var aBool = prefKeyPair.Value as bool?;
                            if (aBool != null)
                            {
                                firefoxoptions.SetPreference(prefKeyPair.Key, aBool.Value);
                                continue;
                            }
                            var anInt = prefKeyPair.Value as int?;
                            if (anInt != null)
                            {
                                firefoxoptions.SetPreference(prefKeyPair.Key, anInt.Value);
                                continue;
                            }
                            var aStr = prefKeyPair.Value as string;
                            if (string.IsNullOrEmpty(aStr)) continue;
                            firefoxoptions.SetPreference(prefKeyPair.Key, aStr);
                        }
                    ret = firefoxoptions;
                    break;
                case SupportedBrowser.InternetExplorer:
                    var ieoptions = new InternetExplorerOptions
                    {
                        BrowserCommandLineArguments = "--private",
                        IntroduceInstabilityByIgnoringProtectedModeSettings = true
                    };
                    ret = ieoptions;
                    break;
                case SupportedBrowser.PhantomJs:
                    ret = null;
                    break;
                default:
                    throw new NotSupportedException(browserType + " is not supported");
            }
            return ret;
        }
    }
}
