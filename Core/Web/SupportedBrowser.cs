﻿namespace Crystalline.Core.Web
{
    public enum SupportedBrowser
    {
        Chrome,
        Firefox,
        InternetExplorer,
        PhantomJs
    };
}
