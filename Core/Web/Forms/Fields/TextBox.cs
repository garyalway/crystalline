﻿using System;
using OpenQA.Selenium;

namespace Crystalline.Core.Web.Forms.Fields
{
    public class TextBox : FormFieldBase
    {
        public TextBox() {}
        public TextBox(IWebElement field) : base(field, null, null, null) { }
        public TextBox(IWebElement field, IWebElement label) : base(field, label, null, null) { }
        public TextBox(IWebElement field, IWebElement label, By fieldRelocator, By fieldLabelRelocator) 
            : base(field, label, fieldRelocator, fieldLabelRelocator) { }        

        protected override void SetFieldValue(string value)
        {
            Clear();
            TheField.SendKeys(value);
        }

        public void Clear()
        {
            if (!TheField.Displayed) return;
            TheField.Clear();
        }

        public TextBox Click()
        {
            if (TheField.Displayed)
                TheField.Click();
            return this;
        }

        public string GetAttribute(string attributeName)
        {
            if (TheField.Displayed)
            try
            {
                return TheField.GetAttribute(attributeName).Trim();
            }
            catch (Exception)
            {
                // ignored
            }
            return "";
        }
    }
}
