﻿using System;
using OpenQA.Selenium;

namespace Crystalline.Core.Web.Forms.Fields
{
    public class CheckBox : FormFieldBase
    {
        public CheckBox(IWebElement field) : base(field, null, null, null) { }
        public CheckBox(IWebElement field, IWebElement label) : base(field, label, null, null) { }
        public CheckBox(IWebElement field, IWebElement label, By fieldRelocator, By fieldLabelRelocator) 
            : base(field, label, fieldRelocator, fieldLabelRelocator) { }

        public bool Checked
        {
            get { return bool.Parse(GetValue()); }
            set { SetFieldValue(value.ToString()); }
        }

        protected override void SetFieldValue(string value)
        {
            // only try to set the value if the current value doesn't match the required new value
            if (bool.Parse(value) != TheField.Selected)
                AttemptToClickElement();
        }

        public override string GetValue()
        {
            return TheField.Selected ? "true" : "false";
        }

        private void AttemptToClickElement()
        {
            // different browsers have different views as to if the actual element is clickable or not
            try
            {
                TheField.Click();
            }
            catch (Exception)
            {
                TheFieldLabel.Click();
            }
        }
    }
}
