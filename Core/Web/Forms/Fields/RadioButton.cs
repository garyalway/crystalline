﻿using System;
using OpenQA.Selenium;

namespace Crystalline.Core.Web.Forms.Fields
{
    public class RadioButton : FormFieldBase
    {
        public RadioButton() { }
        public RadioButton(IWebElement field) : base(field, null, null, null) { }
        public RadioButton(IWebElement field, IWebElement label) : base(field, label, null, null) { }
        public RadioButton(IWebElement field, IWebElement label, By fieldRelocator, By fieldLabelRelocator) 
            : base(field, label, fieldRelocator, fieldLabelRelocator) { }        

        protected override void SetFieldValue(string value)
        {
            // only try to set the value if the current value doesn't match the required new value
            if (bool.Parse(value) == Selected) return;

            // different browsers have different views as to if the actual element is clickable or not
            try
            {
                TheField.Click();
            }
            catch (Exception)
            {
                TheFieldLabel.Click();
            }
        }

        public override string GetValue()
        {
            return TheField.Selected ? "true" : "false";
        }

        public bool Selected
        {
            get
            {
                return TheField.Selected;
            }
        }
    }
}
