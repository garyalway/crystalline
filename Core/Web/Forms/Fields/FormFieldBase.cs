﻿using OpenQA.Selenium;

namespace Crystalline.Core.Web.Forms.Fields
{    
    public abstract class FormFieldBase
    {        
        // use composition instead of inheritance and assume that all field inputs have a label
        protected IWebElement TheField;
        protected IWebElement TheFieldLabel;
        // optional. for use in case of stale elements
        public By FieldRelocator;
        public By FieldLabelRelocator;

        protected FormFieldBase()
        {
        }

        protected FormFieldBase(IWebElement field, IWebElement label, By fieldRelocator, By fieldLabelRelocator)
        {
            TheField = field;
            TheFieldLabel = label;
            FieldRelocator = fieldRelocator;
            FieldLabelRelocator = fieldLabelRelocator;
        }
        
        public bool Displayed
        {
            get
            {
                return TheField.Displayed;
            }
        }

        public IWebElement Label
        {
            get
            {
                return TheFieldLabel;
            }
        }

        public virtual string GetValue()
        {
            return TheField.GetAttribute("value");
        }

        protected abstract void SetFieldValue(string value);

        public void SetValue(string value)
        {
            SetFieldValue(value);
        }
    }
}
