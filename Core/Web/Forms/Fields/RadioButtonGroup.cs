﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace Crystalline.Core.Web.Forms.Fields
{
    public class RadioButtonGroup : FormFieldBase
    {
        public List<RadioButton> RadioButtons = new List<RadioButton>();
        public readonly string GroupName;

        public RadioButtonGroup(string groupName) : this(groupName, null, null) { }
        public RadioButtonGroup(string groupName, IWebElement label) : this(groupName, label, null) { }
        public RadioButtonGroup(string groupName, IWebElement label, By labelRelocator) : base(null, label, null, labelRelocator)
        {
            GroupName = groupName;            
        }

        protected override void SetFieldValue(string value)
        {
            var radio = RadioButtons.Find(x => x.Label.Text.Equals(value, StringComparison.OrdinalIgnoreCase));

            if (radio != null)
                radio.SetValue("true");
            else
                throw new ApplicationException("Option not available");
        }

        public override string GetValue()
        {
            var radio = RadioButtons.Find(x => x.Selected);
            if (radio != null)
                return radio.Label.Text;                
            throw new ApplicationException("Could not identity the selected radio option");
        }
    }
}
