﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Crystalline.Core.Web.Forms.Fields
{
    public class DropDownList : FormFieldBase
    {
        private readonly SelectElement _selectElement;

        public DropDownList()
        {
        }

        public DropDownList(IWebElement field) : this(field, null, null, null) { }
        public DropDownList(IWebElement field, IWebElement label) : this(field, label, null, null) { }

        public DropDownList(IWebElement field, IWebElement label, By fieldRelocator, By fieldLabelRelocator)
            : base(field, label, fieldRelocator, fieldLabelRelocator)
        {
            _selectElement = new SelectElement(TheField);
        }        

        protected override void SetFieldValue(string value)
        {
            if (!TheField.Displayed) return;
            if (string.IsNullOrEmpty(value)) return;
            _selectElement.SelectByText(value);
        }

        public override string GetValue()
        {
            return _selectElement.SelectedOption.Text.Trim();
        }

        public ReadOnlyCollection<IWebElement> GetOptions()
        {
            return TheField.FindElements(By.TagName("option"));
        }

        public List<string> GetOptionsText()
        {
            return GetOptions().Select(x => x.Text).ToList();
        }
    }
}
