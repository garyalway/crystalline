﻿using System;
using Crystalline.Core.Web.Forms.Fields;
using OpenQA.Selenium;

namespace Crystalline.Core.Web.Forms
{
    public class DefaultFieldFactory : IFieldFactory
    {
        public T BuildElement<T>(IWebElement element, IWebElement label) where T : FormFieldBase, new()
        {
            FormFieldBase retVal;
            if ("select".Equals(element.TagName))
            {
                retVal = new DropDownList(element, label);
            }
            else if ("input".Equals(element.TagName))
            {
                var type = element.GetAttribute("type");
                if ("text".Equals(type) || "password".Equals(type) || "email".Equals(type) || "tel".Equals(type) || "number".Equals(type))
                {
                    retVal = new TextBox(element, label);
                }
                else if ("checkbox".Equals(type))
                {
                    retVal = new CheckBox(element, label);
                }
                else if ("radio".Equals(type))
                {
                    retVal = new RadioButton(element, label);
                }
                else
                {
                    throw new Exception("Unexpected input element");
                }
            }
            else
                throw new Exception("Unexpected element tagname, expected either 'select' or 'input'");

            return (T)retVal;
        }

        public RadioButtonGroup BuildRadioButtonGroup(string groupName, IWebElement group, bool resolveRadios)
        {
            var radioButtonGroup = new RadioButtonGroup(groupName, group, By.Id(group.GetAttribute("id")));
            if (resolveRadios)
            {
                foreach (var radio in group.FindElements(By.CssSelector("input[type='radio']")))
                {
                    var label = group.FindElement(By.CssSelector("*[for='" + radio.GetAttribute("id") + "']"));
                    radioButtonGroup.RadioButtons.Add(BuildElement<RadioButton>(radio, label));
                }
            }
            return radioButtonGroup;
        }
    }
}
