﻿using Crystalline.Core.Web.Forms.Fields;
using OpenQA.Selenium;

namespace Crystalline.Core.Web.Forms
{
    public interface IFieldFactory
    {
        T BuildElement<T>(IWebElement element, IWebElement label) where T : FormFieldBase, new();
        RadioButtonGroup BuildRadioButtonGroup(string groupName, IWebElement group, bool resolveRadios);
    }
}
